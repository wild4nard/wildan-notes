import { Layouts } from "../components/Layouts";
import { Navbar } from "../components/Navbar";
import Footer from "../components/Footer";
import AddItems from "../components/AddItems"
export default function Index() {
  return (
    <>
      <Layouts title="PengLink">
        <Navbar />
        <AddItems />
      </Layouts>
      <Footer />
    </>
  )
}
