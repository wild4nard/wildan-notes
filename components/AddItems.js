import {
   AlertDialog,
   AlertDialogBody,
   AlertDialogContent,
   AlertDialogFooter,
   AlertDialogHeader,
   AlertDialogOverlay,
   Box,
   Button,
   Flex,
   FormControl,
   FormLabel,
   HStack,
   Input,
   Link,
   Modal,
   ModalBody,
   ModalCloseButton,
   ModalContent,
   ModalFooter,
   ModalHeader,
   ModalOverlay,
   SimpleGrid,
   Stack,
   Text,
   Textarea,
   useColorMode,
   useDisclosure,
   useToast
} from '@chakra-ui/react'
import { useEffect, useRef, useState } from 'react'
import { FaTrashAlt } from 'react-icons/fa'
import { IoAdd } from 'react-icons/io5'
import { v4 as uuidv4 } from "uuid";
import ListItems from './ListItems';
import { ModalAdd, ModalDelete, ModalShow } from './Modals';
export default function AddItems() {
   const { colorMode } = useColorMode()
   const { isOpen, onOpen, onClose } = useDisclosure()
   const [isopen, setIsOpen] = useState(false)
   const [isopenDel, setIsOpenDel] = useState(false)
   const onCloseDel = () => setIsOpenDel(false)
   const onTutup = () => setIsOpen(false)
   const cancelRef = useRef()
   const toast = useToast()
   const [newNote, setnewNote] = useState('')
   const [newDesc, setnewDesc] = useState('')
   const [notes, setNotes] = useState([])
   const [modalValue, setModalValue] = useState({})
   useEffect(() => {
      const data = localStorage.getItem('notes')
      if (data) {
         setNotes(JSON.parse(data))
      }
   }, [])
   useEffect(() => {
      localStorage.setItem('notes', JSON.stringify(notes))
   })
   function handleSubmit(e) {
      if (newNote == "" || newDesc == "") {
         toast({
            title: "Empty Title or Description",
            description: "Please fill in the column",
            status: "error",
            position: "top",
            duration: 1500,
            isClosable: true,
         })
      } else {
         setNotes([
            ...notes,
            {
               id: uuidv4(),
               title: newNote,
               desc: newDesc
            }
         ])
         setnewNote('')
         setnewDesc('')
         onClose()
         toast({
            title: "Note Created",
            description: "Congratulations, you have successfully made a note",
            status: "success",
            position: "top",
            duration: 2000,
            isClosable: true,
         })
      }
   }
   const deletItem = (id) => {
      const updateItems = notes.filter((note) => {
         return note.id !== id
      })
      setNotes(updateItems)
      onCloseDel()
      toast({
         title: `Successfully deleted`,
         status: "success",
         position: "top",
         duration: 2000,
         isClosable: true,
      })
   }
   function handleDel(note) {
      setIsOpenDel(true)
      setModalValue(note)
   }
   function handleEdit(note) {
      setIsOpen(true)
      setModalValue(note)
   }
   return (
      <>
         <Flex
            justifyContent="flex-end"
         >
            <Button
               leftIcon={<IoAdd />}
               boxShadow="md"
               shadow="md"
               bgColor={colorMode === 'light' ? '#d8642d' : 'gray.700'}
               color="white"
               onClick={onOpen}
            >
               Note
            </Button>
         </Flex >
         <ModalAdd
            openAdd={isOpen}
            closeAdd={onClose}
            title={newNote}
            tarTitle={(e) => setnewNote(e.target.value)}
            desc={newDesc}
            tarDesc={(e) => setnewDesc(e.target.value)}
            saveBtn={handleSubmit}
         />
         <Box
            mt={5}
            w="100%"
            border="1px"
            borderRadius="10px"
            borderColor={colorMode === 'light' ? '#d8642d' : 'gray.700'}
            p={4}
            borderStyle="dashed"
            overflow="hidden"
         >
            <SimpleGrid
               columns={4}
               spacing={4}
               display={{ base: "block", md: "grid" }}
            >
               {
                  notes.map((note, i) => (
                     <Box
                        key={i}
                        p={2}
                        borderRadius={10}
                        border="1px"
                        borderColor={colorMode === 'light' ? 'gray.300' : 'gray.700'}
                        boxShadow="based"
                        mb={{ base: 4, md: 0 }}
                     >
                        <Text isTruncated>
                           <Link onClick={() => handleEdit(note)}>
                              {note.title}
                           </Link>
                        </Text>
                        <HStack
                           mt={4}
                           justifyContent="flex-end"
                           alignContent="flex-end"

                        >
                           <Button
                              size="xs"
                              bgColor={colorMode === 'light' ? '#d8642d' : '#FF4848'}
                              color="white"
                              onClick={() => handleDel(note)}
                           ><FaTrashAlt /></Button>
                        </HStack>
                     </Box>
                  ))
               }
            </SimpleGrid>
         </Box>
         <ModalDelete
            openDel={isopenDel}
            cancelDel={cancelRef}
            closeDel={onCloseDel}
            valueDel={() => deletItem(modalValue.id)}
         />
         <ModalShow
            openShow={isopen}
            closeShow={onTutup}
            valueShowTitle={modalValue.title}
            valueShowDesc={modalValue.desc}
         />
      </>
   )
}
