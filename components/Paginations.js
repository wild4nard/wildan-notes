import { Flex } from '@chakra-ui/react'
import { useState } from 'react';
import Pagination from 'react-pagination-library'
import 'react-pagination-library/build/css/index.css'
export default function Paginations() {
   const [state, setState] = useState('')
   const changeCurrentPage = (numPage) => {
      setState({ currentPage: numPage });
   };
   return (
      <Flex
         justifyContent="center"
         alignItems="center"
      >
         <Pagination
            currentPage={state.currentPage}
            totalPages={10}
            changeCurrentPage={changeCurrentPage}
            theme="square-i"

         />
      </Flex>
   )
}
