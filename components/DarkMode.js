import { IconButton, useColorMode } from '@chakra-ui/react'
import React from 'react'
import { FaRegMoon, FaRegSun } from 'react-icons/fa';
export const DarkMode = () => {
   const { colorMode, toggleColorMode } = useColorMode();
   return (
      <>
         <IconButton
            aria-label="Toogle Dark Mode"
            icon={colorMode === 'light' ? <FaRegMoon /> : <FaRegSun />}
            onClick={toggleColorMode}
            boxShadow="md"
         />
      </>
   )
}
