import { HStack, Link, Text } from '@chakra-ui/react'
import React from 'react'

export default function Footer() {
   return (
      <HStack
         justifyContent="center"
         alignItems="center"
         p={5}
      >
         <Text
            fontSize="sm"
         >Create By </Text>
         <Link
            href="https://www.linkedin.com/in/wildan-ardiansyah/"
            target="_blank"
            color="blue.300"
            fontSize="sm"
         >
            Muhammad WIldan Ardiansyah
         </Link>
      </HStack>
   )
}
