import { Box, Heading, HStack, Spacer, useColorMode } from '@chakra-ui/react'
import React from 'react'
import { DarkMode } from './DarkMode'

export const Navbar = () => {
   const { colorMode } = useColorMode();
   return (
      <>
         <HStack
            py={5}
         >
            <Heading
               as="h2"
               size="2xl"
               fontWeight="bold"
               color={colorMode === 'light' ? '#d8642d' : '#FF4848'}
            >
               Notes
            </Heading>
            <Spacer />
            <Box>
               <DarkMode />
            </Box>
         </HStack>
      </>
   )
}
