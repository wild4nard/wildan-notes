import {
   AlertDialog,
   AlertDialogBody,
   AlertDialogContent,
   AlertDialogFooter,
   AlertDialogHeader,
   AlertDialogOverlay,
   Button,
   FormControl,
   FormLabel,
   Input,
   Modal,
   ModalBody,
   ModalCloseButton,
   ModalContent,
   ModalFooter,
   ModalHeader,
   ModalOverlay,
   Textarea
} from "@chakra-ui/react"

export const ModalShow = ({ openShow, closeShow, valueShowTitle, valueShowDesc }) => {
   return (
      <>
         <Modal
            isOpen={openShow}
            onClose={closeShow}
            size="2xl"
         >
            <ModalOverlay />
            <ModalContent>
               <ModalHeader>Show</ModalHeader>
               <ModalCloseButton />
               <ModalBody>
                  <FormControl
                     border="1px"
                     borderColor="gray.600"
                     borderRadius={10}
                  >
                     <FormLabel
                        m={0}
                        pt={1}
                        px={2}
                        fontWeight="bold"
                     >Title</FormLabel>
                     <Input px={2} pb={2} value={valueShowTitle} readOnly border="none" />
                  </FormControl>
                  <FormControl
                     mt={4}
                     border="1px"
                     borderColor="gray.600"
                     borderRadius={10}>
                     <FormLabel
                        m={0}
                        pt={1}
                        px={2}
                        fontWeight="bold"
                     >Description</FormLabel>
                     <Textarea readOnly border="none" pt={0} px={2} pb={2} rows={5} resize="none" value={valueShowDesc} />
                  </FormControl>
               </ModalBody>
               <ModalFooter>
                  <Button colorScheme="blue" mr={3} onClick={closeShow}>
                     Close
                  </Button>
               </ModalFooter>
            </ModalContent>
         </Modal>
      </>
   )
}
export const ModalDelete = ({ openDel, cancelDel, closeDel, valueDel }) => {
   return (
      <>
         <AlertDialog
            isOpen={openDel}
            leastDestructiveRef={cancelDel}
            onClose={closeDel}
         >
            <AlertDialogOverlay>
               <AlertDialogContent>
                  <AlertDialogHeader fontSize="lg" fontWeight="bold">
                     Delete Note
                  </AlertDialogHeader>

                  <AlertDialogBody>
                     Are you sure you want to delete it?
                  </AlertDialogBody>

                  <AlertDialogFooter>
                     <Button ref={cancelDel} onClick={closeDel}>
                        Cancel
                     </Button>
                     <Button colorScheme="red" onClick={valueDel} ml={3}>
                        Delete
                     </Button>
                  </AlertDialogFooter>
               </AlertDialogContent>
            </AlertDialogOverlay>
         </AlertDialog>
      </>
   )
}
export const ModalAdd = ({ openAdd, closeAdd, title, tarTitle, desc, tarDesc, saveBtn }) => {
   return (
      <>
         <Modal
            isOpen={openAdd}
            onClose={closeAdd}
            size="2xl"
         >
            <ModalOverlay />
            <ModalContent>
               <ModalHeader>Add Note</ModalHeader>
               <ModalCloseButton />
               <ModalBody>
                  <FormControl>
                     <FormLabel>Title</FormLabel>
                     <Input value={title} onChange={tarTitle} />
                  </FormControl>
                  <FormControl mt={4}>
                     <FormLabel>Description</FormLabel>
                     <Textarea rows={5} resize="none" value={desc} onChange={tarDesc} />
                  </FormControl>
               </ModalBody>
               <ModalFooter>
                  <Button colorScheme="green" mr={3} onClick={saveBtn}>
                     Save
                  </Button>
                  <Button colorScheme="blue" mr={3} onClick={closeAdd}>
                     Close
                  </Button>
               </ModalFooter>
            </ModalContent>
         </Modal>
      </>
   )
}

