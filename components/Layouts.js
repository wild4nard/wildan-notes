/* eslint-disable @next/next/no-page-custom-font */
import { Box, useColorMode } from "@chakra-ui/react"
import Head from 'next/head'
export const Layouts = ({ children, title }) => {
   return (
      <>
         <Head>
            <title>{title}</title>
            <meta name="description" content="Pengingat Link"></meta>
         </Head>
         <Box
            width="100%"
            mx="auto"
            maxW="container.lg"
            px="1rem"
         >
            {children}
         </Box>
      </>
   )
}
